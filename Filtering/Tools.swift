//
//  Tools.swift
//  Filtering
//
//  Created by Sergei Perevoznikov on 12.03.18.
//  Copyright © 2018 Sergei Perevoznikov. All rights reserved.
//

import Cocoa

public class Tools {
    public class func filtering<T: Hashable>(from seq: [[T]], values: [T]) -> [[T]] {
        return seq.filter { Set(values).isSubset(of: Set($0)) }
    }
    
    public class func filtering2<T: Hashable & Comparable>(from seq: [[T]], values: [T]) -> [T] {
        return Array(Set(filtering(from: seq, values: values).flatMap({ $0 }))).sorted(by: <)
    }
}
