//
//  FilteringTests.swift
//  FilteringTests
//
//  Created by Sergei Perevoznikov on 12.03.18.
//  Copyright © 2018 Sergei Perevoznikov. All rights reserved.
//

import XCTest
@testable import Filtering

class FilteringTests: XCTestCase {
    //TODO: random test data
    
    private let sequence = [["a", "b", "c"], ["d", "a", "g"], ["d", "e", "g"]]
    private let vals1 = ["a"]
    private let vals2 = ["a", "g"]
    private let vals3 = ["g", "a"]
    
    func test_filtered_count_with_vals_1() {
        let result = Tools.filtering(from: sequence, values: vals1)
        XCTAssertEqual(result.count, 2)
    }
    
    func test_first_filtered_result_with_vals_1() {
        let result = Tools.filtering(from: sequence, values: vals1)
        XCTAssertEqual(result[0], ["a", "b", "c"])
    }
    
    func test_second_filtered_result_with_vals_1() {
        let result = Tools.filtering(from: sequence, values: vals1)
        XCTAssertEqual(result[1], ["d", "a", "g"])
    }
    
    func test_filtered_count_with_vals_2() {
        let result = Tools.filtering(from: sequence, values: vals2)
        XCTAssertEqual(result.count, 1)
    }
    
    func test_filtered_result_with_vals_2() {
        let result = Tools.filtering(from: sequence, values: vals2)
        XCTAssertEqual(result[0], ["d", "a", "g"])
    }
    
    func test_filtered_count_with_vals_3() {
        let result = Tools.filtering(from: sequence, values: vals3)
        XCTAssertEqual(result.count, 1)
    }
    
    func test_filtered_result_with_vals_3() {
        let result = Tools.filtering(from: sequence, values: vals3)
        XCTAssertEqual(result[0], ["d", "a", "g"])
    }
    
    func test_empty_filtered_result_with_vals_1() {
        let result = Tools.filtering(from: [], values: vals1)
        XCTAssertEqual(result.count, 0)
    }
    
    func test_empty_filtered_result_with_vals_2() {
        let result = Tools.filtering(from: [], values: vals2)
        XCTAssertEqual(result.count, 0)
    }
    
    func test_flat_filtered_result_with_vals_1() {
        let result = Tools.filtering2(from: sequence, values: vals1)
        XCTAssertEqual(result, ["a", "b", "c", "d", "g"])
    }
    
    func test_flat_filtered_result_with_vals_2() {
        let result = Tools.filtering2(from: sequence, values: vals2)
        XCTAssertEqual(result, ["a", "d", "g"])
    }
}
